# Ubuntu 16.04 [![build status](https://gitlab.com/nvidia/cuda-ppc64le/badges/ubuntu16.04/build.svg)](https://gitlab.com/nvidia/cuda-ppc64le/commits/ubuntu16.04)

## CUDA 8.0
- [`8.0-runtime`, `8.0-runtime-ubuntu16.04` (*8.0/runtime/Dockerfile*)](https://gitlab.com/nvidia/cuda-ppc64le/blob/ubuntu16.04/8.0/runtime/Dockerfile)
- [`8.0-devel`, `latest`, `8.0-devel-ubuntu16.04` (*8.0/devel/Dockerfile*)](https://gitlab.com/nvidia/cuda-ppc64le/blob/ubuntu16.04/8.0/devel/Dockerfile)
- [`8.0-cudnn6-runtime`, `8.0-cudnn6-runtime-ubuntu16.04` (*8.0/runtime/cudnn6/Dockerfile*)](https://gitlab.com/nvidia/cuda-ppc64le/blob/ubuntu16.04/8.0/runtime/cudnn6/Dockerfile)
- [`8.0-cudnn6-devel`, `8.0-cudnn6-devel-ubuntu16.04` (*8.0/devel/cudnn6/Dockerfile*)](https://gitlab.com/nvidia/cuda-ppc64le/blob/ubuntu16.04/8.0/devel/cudnn6/Dockerfile)
- [`8.0-cudnn5-runtime`, `8.0-cudnn5-runtime-ubuntu16.04` (*8.0/runtime/cudnn5/Dockerfile*)](https://gitlab.com/nvidia/cuda-ppc64le/blob/ubuntu16.04/8.0/runtime/cudnn5/Dockerfile)
- [`8.0-cudnn5-devel`, `8.0-cudnn5-devel-ubuntu16.04` (*8.0/devel/cudnn5/Dockerfile*)](https://gitlab.com/nvidia/cuda-ppc64le/blob/ubuntu16.04/8.0/devel/cudnn5/Dockerfile)
